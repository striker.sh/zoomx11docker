# Usage

## Build Image
```
git clone https://gitlab.com/striker.sh/zoomx11docker
cd ZoomX11Docker
docker build -t x11_zoom .
```
## start zoom
```
x11docker --pulseaudio zoom
```

# Dependencies

- [x11docker](https://github.com/mviereck/x11docker)
- [x11docker dependencies](https://github.com/mviereck/x11docker/wiki/Dependencies#dependencies-of-feature-options)

# Why?

To isolate zoom

# What hasn't been tested

Webcam and screen sharing

if you want to try webcam add `--webcam` to `x11docker` command and install adequate dependencies

